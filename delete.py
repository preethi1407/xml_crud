import xml.etree.ElementTree as ET

# parsing the xml document
tree = ET.parse('students_info.xml')
root = tree.getroot()

# Element.findall() finds only elements with a tag which are direct children of the current element. 
for student in root.findall('student'):

# Element.find() finds the first child with a particular tag
	rank = int(student.find('rank').text)
	if rank > 5:

# removing elements using Element.remove()
		root.remove(student)

tree.write('deleted_output.xml')
