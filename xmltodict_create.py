from dicttoxml import dicttoxml
Student = {
  "student1" : {
    "student_name" : "preethi",
    "branch": "ECE",
    "id" : 1
  },
  "student2" : {
    "student_name" : "sowmya",
    "branch": "CSE",
    "id" : 2
  },
  "student3" : {
    "student_name" : "bala",
    "branch": "EEE",
    "id" : 3
  }
} 

# converting dictionary to xml format
xml=dicttoxml(Student)

# opening a new file for writing
xmlfile = open("students.xml", "w")

# writing into opened file after decoding it 
xmlfile.write(xml.decode())

xmlfile.close()
