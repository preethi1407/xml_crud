import xmltodict

#opening the xml file
with open('students.xml', 'r', encoding='utf-8') as file:

# reading the contents of the opened xml file
  my_xml = file.read()

#Using xmltodict module to parse and convert the XML document into a dictionary
my_dict = xmltodict.parse(my_xml)

# printing the dictionary
print(my_dict)

