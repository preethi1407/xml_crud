# importing data by reading from a xml file
import xml.etree.ElementTree as ET
tree = ET.parse('students_info.xml')
root = tree.getroot()

#incrementing the age for each iteration
for age in root.iter('age'):
	newAge = int(age.text)+1
	age.text=str(newAge)
	age.set('changed', 'yes')

# writing the changes into a new xml file
tree.write("students_info_new.xml") 

#Insert new field into <fields>
data = ET.tostring(root,encoding = "utf8",method = "xml")
root = ET.fromstring(data)

# entering the details of new field to be created
new_field = ET.Element("student",name="reshma")
ET.SubElement(new_field, 'id').text = "4"
ET.SubElement(new_field, 'age').text = "17"
ET.SubElement(new_field, 'class').text = "12"
ET.SubElement(new_field, 'rank').text = "9"
root.insert(3, new_field)

# adding second new field to the existing data
new_field = ET.Element("student",name="neelima")
ET.SubElement(new_field, 'id').text = "5"
ET.SubElement(new_field, 'age').text = "17"
ET.SubElement(new_field, 'class').text = "12"
ET.SubElement(new_field, 'rank').text = "9"
root.insert(4, new_field)

ET.dump(root)
tree = ET.ElementTree(root)
tree.write(open('updated_info.xml','w'), encoding='unicode')

